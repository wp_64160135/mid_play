import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("Message", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const showMessage = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  };
  const closeMessage = (msg: string) => {
    message.value = msg;
    isShow.value = false;
  };
  return { isShow, message, showMessage, closeMessage, timeout };
});
