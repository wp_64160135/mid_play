import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";

export const useLoginStore = defineStore("login", () => {
  const messageStore = useMessageStore();
  const userStore = useUserStore();
  const loginName = ref("");
  const loginPhoto = ref("");
  const isLogin = computed(() => {
    //loginName is not empty
    return loginName.value !== "";
  });
  const login = (userName: string, password: string): void => {
    if (userStore.login(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
      localStorage.setItem("loginPhoto");
    } else {
      messageStore.showMessage("Login หรือ Password ไม่ถูกต้อง");
    }
  };
  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { isLogin, login, logout, loadData, loginName, loginPhoto };
});
