import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
// import type { VSelect } from "vuetify/components";

export const useProductStore = defineStore("product", () => {
  // const Item = ref("");
  // const valid = ref(true);
  // const buyItem = async () => {
  //   const form = ref<InstanceType<typeof VSelect> | null>(null);
  //   const {valid} = await form.value!.validate();
  const list = ref<Product[]>([]);
  const addMenu = (item: {
    id: string;
    name: string;
    price: number;
    qty: number;
    photo: string;
  }) => {
    if (list.value.includes(item)) {
      item.qty = item.qty + 1;
    } else {
      list.value.push(item);
    }
  };

  const coffee = ref<Product[]>([
    {
      id: "c1",
      name: "มอคค่า",
      price: 55,
      qty: 1,
      photo:
        "https://www.bluemochatea.com/wp-content/webp-express/webp-images/uploads/2020/02/IMG_2614-1024x1024.jpg.webp",
    },
    {
      id: "c2",
      name: "คาปูชิโน",
      price: 50,
      qty: 1,
      photo:
        "https://s359.thaibuffer.com/pagebuilder/d5725442-1458-4a0e-83b2-2ec0e6908d3d.jpg",
    },
    {
      id: "c3",
      name: "ลาเต้",
      price: 50,
      qty: 1,
      photo:
        "https://s359.thaibuffer.com/pagebuilder/95bcea87-d77c-4236-9cb2-39229c61b15c.jpg",
    },
    {
      id: "c4",
      name: "เอสเพรสโซ",
      price: 50,
      qty: 1,
      photo:
        "https://santipanich.com/wp-content/uploads/2021/05/closeup-classic-fresh-espresso-served-dark-surface.jpg",
    },
    {
      id: "c5",
      name: "มัคคิอาโต้",
      price: 50,
      qty: 1,
      photo:
        "https://www.nestleprofessional.co.th/sites/default/files/styles/np_recipe_detail/public/2022-04/nescafe-iced-caramel-macchiato-540x400%20%281%29.jpg?itok=u3jWdrDl",
    },
    {
      id: "c6",
      name: "อเมริกาโน่",
      price: 45,
      qty: 1,
      photo:
        "https://s359.thaibuffer.com/pagebuilder/da0135fc-46e5-485f-abe8-63dda5b39005.jpg",
    },
    {
      id: "c7",
      name: "คาราเมลแมคคิอาโต้",
      price: 55,
      qty: 1,
      photo:
        "https://www.nestleprofessional.co.th/sites/default/files/styles/np_recipe_detail/public/2022-04/nescafe-iced-caramel-macchiato-540x400%20%281%29.jpg?itok=u3jWdrDl",
    },
    {
      id: "c8",
      name: "กาแฟยูซุ",
      price: 50,
      qty: 1,
      photo:
        "https://www.nescafe.com/th/sites/default/files/2022-04/Americano-Shaken-Yuzu-1212x680.jpg",
    },
    {
      id: "c9",
      name: "กาแฟคัลโกน่า",
      price: 60,
      qty: 1,
      photo: "https://www.thaismescenter.com/wp-content/uploads/2020/06/93.jpg",
    },
  ]);

  const juice = ref<Product[]>([
    {
      id: "j1",
      name: "น้ำส้ม",
      price: 60,
      qty: 1,
      photo: "https://cdn1.productnation.co/stg/sites/6/5c88d54e034e2.jpeg",
    },
    {
      id: "j2",
      name: "น้ำแอ๊ปเปิ้ล",
      price: 60,
      qty: 1,
      photo:
        "https://1.bp.blogspot.com/-a6Y8IsNzY-0/YJEmVtojunI/AAAAAAAAgik/SGPA8MXcqsQe6_cHCuKm2AqVRUDpE-8OACLcBGAsYHQ/w1200-h630-p-k-no-nu/%25E0%25B8%2599%25E0%25B9%2589%25E0%25B8%25B3%25E0%25B9%2581%25E0%25B8%25AD%25E0%25B8%259B%25E0%25B9%2580%25E0%25B8%259B%25E0%25B8%25B4%25E0%25B9%2589%25E0%25B8%25A5.jpg",
    },
    {
      id: "j3",
      name: "น้ำสับปะรด",
      price: 60,
      qty: 1,
      photo:
        "https://obs.line-scdn.net/0hHm0Zj6r1F0INIz2Lw8FoFTd1FC0-TwRBaRVGQVFNSXZyQwRGMxZdLC4lQSUlRlAcZEZfJiwjDHMoRwBAYRVc/w644",
    },
    {
      id: "j4",
      name: "น้ำองุ่น",
      price: 60,
      qty: 1,
      photo: "https://img.kapook.com/u/surauch/cook/grape.jpg",
    },
    {
      id: "j5",
      name: "น้ำแตงโม",
      price: 60,
      qty: 1,
      photo:
        "https://img.wongnai.com/p/1968x0/2019/03/26/86eb7649b17d4f0383a6e584f305506c.jpg",
    },
    {
      id: "j6",
      name: "น้ำลูกพรุน",
      price: 60,
      qty: 1,
      photo:
        "https://img.kapook.com/u/surauch/movie2/shutterstock_62788912.jpg ",
    },
    {
      id: "j7",
      name: "น้ำแครอท",
      price: 60,
      qty: 1,
      photo:
        "https://img.kapook.com/u/pirawan/Cooking1/carot%20mango%20spicy.jpg",
    },
    {
      id: "j8",
      name: "น้ำเบอร์รี่ ",
      price: 60,
      qty: 1,
      photo:
        "https://xn--q3cp7eza.net/media/images/recipe/%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%A1%E0%B8%B1%E0%B8%A5%E0%B9%80%E0%B8%9A%E0%B8%AD%E0%B8%A3%E0%B9%8C%E0%B8%A3%E0%B8%B5%E0%B9%88.jpg",
    },
    {
      id: "j9",
      name: "น้ำมะเขือเทศ",
      price: 60,
      qty: 1,
      photo:
        "https://s359.kapook.com/pagebuilder/f8047767-e547-4e24-b734-c0397b7aeef0.jpg ",
    },
  ]);
  const smoothies = ref<Product[]>([
    {
      id: "s1",
      name: "สมูทตี้ มิกซ์เบอร์รี่",
      price: 60,
      qty: 1,
      photo:
        "https://image.sistacafe.com/w800/images/uploads/summary/image/30510/d702b10b-cab7-4bbd-92a6-6cf53d2131b6.jpg",
    },
    {
      id: "s2",
      name: "สมูทตี้กี่วี กล้วย โยเกิร์ต",
      price: 60,
      qty: 1,
      photo: "https://www.thai-thaifood.com/bilder/3994c.jpg",
    },
    {
      id: "s3",
      name: "สมูทตี้อะโวคาโด",
      price: 60,
      qty: 1,
      photo:
        "https://images-ext-1.discordapp.net/external/eQXZ-U62qwkywdrqrrtTEwVuXWBjhIcqXN5UwOnp2ZI/https/images.deliveryhero.io/image/fd-th/LH/kcag-hero.jpg?width=917&height=671",
    },
    {
      id: "s4",
      name: "สมูทตี้สตอเบอร์รี่โยเกิร์ต",
      price: 60,
      qty: 1,
      photo:
        "https://image.sistacafe.com/images/uploads/summary/image/4020/1445910237-healthysmoothies_article_1_.jpg",
    },
    {
      id: "s5",
      name: "สมูทตี้ส้ม + มะม่วง",
      price: 60,
      qty: 1,
      photo:
        "https://files.vogue.co.th/uploads/fresh-mango-smoothie_1339-1485.jpg",
    },
    {
      id: "s6",
      name: "สมูทตี้ฝรั่ง",
      price: 60,
      qty: 1,
      photo: "https://www.akerufeed.com/wp-content/uploads/2019/03/1-25.jpg ",
    },
    {
      id: "s7",
      name: "เเมงโก้สมูทตี้",
      price: 60,
      qty: 1,
      photo: "https://cooking.teenee.com/smoothie/img1/1328.jpg",
    },
    {
      id: "s8",
      name: "สมูทตี้แอปเปิ้ล",
      price: 60,
      qty: 1,
      photo:
        "https://www.matichon.co.th/wp-content/uploads/2018/07/%E0%B8%AA%E0%B8%A1%E0%B8%B9%E0%B9%8A%E0%B8%97%E0%B8%95%E0%B8%B5%E0%B9%89%E0%B9%81%E0%B8%AD%E0%B8%9B%E0%B9%80%E0%B8%9B%E0%B8%B4%E0%B9%89%E0%B8%A5.jpg",
    },
    {
      id: "s9",
      name: "เชอร์รี่สมูทตี้",
      price: 60,
      qty: 1,
      photo:
        "https://static.trueplookpanya.com/tppy/member/m_557500_560000/559387/cms/images/%E0%B8%AA%E0%B8%B9%E0%B8%95%E0%B8%A3%E0%B8%AA%E0%B8%A1%E0%B8%B9%E0%B8%97%E0%B8%95%E0%B8%B5%E0%B9%89%E0%B8%A7%E0%B8%B4%E0%B8%95%E0%B8%B2%E0%B8%A1%E0%B8%B4%E0%B8%99%E0%B8%AA%E0%B8%B9%E0%B8%87_15.jpg",
    },
  ]);
  const bakery = ref<Product[]>([
    {
      id: "b1",
      name: "เค้กกล้วยหอม",
      price: 60,
      qty: 1,
      photo:
        "https://img.wongnai.com/p/1600x0/2021/08/17/8e05b25980aa462d9ed0be94c1490e14.jpg",
    },
    {
      id: "b2",
      name: "เค้กไข่ไต้หวัน",
      price: 60,
      qty: 1,
      photo:
        "https://panleebakery.com/wp-content/uploads/2019/07/original-cake-snackbox-%E0%B8%AA%E0%B9%81%E0%B8%99%E0%B9%87%E0%B8%84%E0%B8%9A%E0%B9%8A%E0%B8%AD%E0%B8%81%E0%B8%8B%E0%B9%8C-%E0%B8%88%E0%B8%B1%E0%B8%94%E0%B9%80%E0%B8%A5%E0%B8%B5%E0%B9%89%E0%B8%A2%E0%B8%87-catering_%E0%B8%AA%E0%B8%B1%E0%B8%A1%E0%B8%A1%E0%B8%99%E0%B8%B2-%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%8A%E0%B8%B8%E0%B8%A1-%E0%B8%82%E0%B8%99%E0%B8%A1%E0%B8%9B%E0%B8%B1%E0%B8%87-%E0%B8%82%E0%B8%99%E0%B8%A1%E0%B9%80%E0%B8%9A%E0%B8%A3%E0%B8%84-%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B8%A7%E0%B9%88%E0%B8%B2%E0%B8%87-lunchbox-CoffeeBreak-foodstallmenu.jpg",
    },
    {
      id: "b3",
      name: "เค้กฝอยทองครีมสดมะพร้าวอ่อน",
      price: 60,
      qty: 1,
      photo: "https://f.ptcdn.info/612/057/000/p8t0ttje5oDNSDvzEr8-o.jpg",
    },
    {
      id: "b4",
      name: "บลูเบอร์รีครัมเบิลมัฟฟิน",
      price: 60,
      qty: 1,
      photo: "https://f.ptcdn.info/956/076/000/ra69ju1i68vDZEqSGRmbd-o.jpg",
    },
    {
      id: "b5",
      name: "ขนมปังลาวาชาเขียว",
      price: 60,
      qty: 1,
      photo: "https://i.ytimg.com/vi/SO5ufmq4eko/maxresdefault.jpg",
    },
    {
      id: "b6",
      name: "คุกกี้ลาวากระทะร้อน",
      price: 60,
      qty: 1,
      photo:
        "https://pbs.twimg.com/media/DTo0FQRUMAAqjTI?format=jpg&name=large",
    },
    {
      id: "b7",
      name: "คุกกี้สิงคโปร์",
      price: 60,
      qty: 1,
      photo:
        "https://www.kanomsongsan.com/Images/Products/Large/6866721-20200416-223552.jpg",
    },
    {
      id: "b8",
      name: "ทาร์ตผลไม้",
      price: 60,
      qty: 1,
      photo: "https://f.ptcdn.info/009/067/000/q1f5qe58m19HzrtsO25Q-o.jpg",
    },
    {
      id: "b9",
      name: "ขนมปังช็อกโกแลตเนยสด",
      price: 60,
      qty: 1,
      photo:
        "https://www.easycookingmenu.com/media/k2/items/cache/505a5676b5a3aa6a3f6b2efa15ce8a2c_XL.jpg",
    },
  ]);
  const softdrink = ref<Product[]>([
    {
      id: "sd1",
      name: "โค้ก",
      price: 60,
      qty: 1,
      photo:
        "https://st.bigc-cs.com/public/media/catalog/product/18/88/8855199141018/8855199141018.jpg",
    },
    {
      id: "sd2",
      name: "น้ำแดง",
      price: 60,
      qty: 1,
      photo:
        "https://backend.tops.co.th/media/catalog/product/8/8/8851959141175_04-03-2022.jpg",
    },
    {
      id: "sd3",
      name: "เป๊ปซี่",
      price: 60,
      qty: 1,
      photo:
        "https://backend.tops.co.th/media/catalog/product/8/8/8858998581214_30-07-2021.jpg",
    },
    {
      id: "sd4",
      name: "สไปร์ท",
      price: 60,
      qty: 1,
      photo:
        "https://backend.tops.co.th/media/catalog/product/8/8/8851959144367_05-03-2021.jpg",
    },
    {
      id: "sd5",
      name: "น้ำเขียว",
      price: 60,
      qty: 1,
      photo:
        "https://backend.tops.co.th/media/catalog/product/8/8/8851959144183.jpg",
    },
    {
      id: "sd6",
      name: "น้ำส้มแฟนต้า",
      price: 60,
      qty: 1,
      photo:
        "https://backend.tops.co.th/media/catalog/product/8/8/8851959144169_1.jpg",
    },
    {
      id: "sd7",
      name: " น้ำแดงมะนาวโซดา",
      price: 60,
      qty: 1,
      photo:
        "https://image.bestreview.asia/wp-content/uploads/2022/08/best-SINGHA-3.jpg",
    },
    {
      id: "sd8",
      name: "เลม่อนโซดา",
      price: 60,
      qty: 1,
      photo:
        "https://backend.tops.co.th/media/catalog/product/8/8/8850999016863.jpg",
    },
    {
      id: "sd9",
      name: "น้ำองุ่น",
      price: 60,
      qty: 1,
      photo:
        "https://backend.tops.co.th/media/catalog/product/8/8/8851959144190_1.jpg",
    },
  ]);

  return {
    coffee,
    juice,
    smoothies,
    bakery,
    softdrink,
    list,
    addMenu,
  };
});
