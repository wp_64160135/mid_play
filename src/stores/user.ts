import { ref } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useUserStore = defineStore("user", () => {
  const isTable = ref(true);
  const dialog = ref(false);
  const editedUser = ref<User>({
    id: -1,
    login: "",
    name: "",
    password: "",
    photo: "",
  });

  const clearUser = () => {
    editedUser.value = {
      id: -1,
      login: "",
      name: "",
      password: "",
      photo: "",
    };
  };
  let lastId = 4;
  const users = ref<User[]>([
    {
      id: 1,
      login: "admin",
      name: "Administrator",
      password: "Pass@1234",
      photo: "https://fbi.dek-d.com/27/0394/1144/122574935",
    },
    {
      id: 2,
      login: "user1",
      name: "User 1",
      password: "Pass@1234",
      photo: "https://fbi.dek-d.com/27/0394/1144/122574935",
    },
    {
      id: 3,
      login: "user2",
      name: "User 2",
      password: "Pass@1234",
      photo: "https://fbi.dek-d.com/27/0394/1144/122574935",
    },
  ]);

  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.login === loginName);
    if (index >= 0) {
      const user = users.value[index];
      if (user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };

  const saveUser = () => {
    if (editedUser.value.id < 0) {
      //Add new
      editedUser.value.id = lastId++;
      users.value.push(editedUser.value);
    } else {
      const index = users.value.findIndex(
        (item) => item.id === editedUser.value.id
      );
      users.value[index] = editedUser.value;
    }
    dialog.value = false;
    clearUser();
  };

  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id === id);
    users.value.splice(index, 1);
  };

  const editUser = (user: User) => {
    editedUser.value = { ...user }; // JSON.parse(JSON.stringify(user)) or { ...user }  การระเบิดตัว object ออกมา
    dialog.value = true;
  };
  return {
    users,
    deleteUser,
    dialog,
    editedUser,
    clearUser,
    saveUser,
    editUser,
    isTable,
    login,
  };
});
